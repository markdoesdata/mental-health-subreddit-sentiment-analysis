# Mental health subreddit sentiment analysis

This repo is the source code for a sentiment analysis that was published on Medium in 2018.

## Link to article
[What happened on May 15, 2012?](https://medium.com/@mawilson_62057/what-happened-on-may-15-2012-b28126b0ae38)

## License
[MIT](https://choosealicense.com/licenses/mit/)
